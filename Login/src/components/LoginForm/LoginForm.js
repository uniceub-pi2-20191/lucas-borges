import React, { Component } from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, Text, Alert } from 'react-native';

export default class FakeLogintInput extends Component {

  render() {
    return (
        <View>
			  	<TextInput
			        style={styles.hoho}
              placeholder='Email'
              autoCapitalize='none'
              autoCorrect={false}
              keyboardType='email-address'
			    />

          <TextInput
              style={styles.hoho}
              placeholder='Senha'
              secureTextEntry
          />
        <TouchableOpacity style={styles.button} onPress={() => {Alert.alert("HAHA!","Não levarei SR!")}}>
          <Text style={styles.textButton}> Login </Text>
        </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({

  hoho: {

    height: 40,
    borderColor: 'gray',
    borderWidth: 2,
    backgroundColor: '#fff'

  },

  button: {
    height: 50,
    borderWidth: 3,
    backgroundColor: '#ffff69'
  },

  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 30

  }

});