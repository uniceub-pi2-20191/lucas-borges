import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import LoginForm from '../LoginForm/LoginForm';


export default class Login extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/logo.jpg')}/>
					<LoginForm/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
  	backgroundColor: '#2FB320',
  	flex: 1,
  },

  inner: {
    flexDirection: 'column',
	justifyContent: 'center', 
	alignItems: 'center', 
	height: '100%',

  },

  image: {
  	width:  100, 
  	height: 100,
  },
});

