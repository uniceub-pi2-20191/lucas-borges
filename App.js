import React, { Component } from 'react';
import { AppRegistry, Image, View, Text } from 'react-native';

export default class BananasBoliche extends Component {
  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    let pic2 = {
      uri: 'https://img.elo7.com.br/product/zoom/1F65285/aplique-adesivo-tag-bola-boliche-enfeite.jpg'
    };
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{alignItems: "center" }}>
          <Text>Banana</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <Image source={pic} style={{width: 193, height: 100}}/>
        </View>
        <View style={{alignItems: "center" }}>
          <Text>Boliche</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <Image source={pic2} style={{width: 193, height: 100}}/>
        </View>
      </View>
    );
  }
}
